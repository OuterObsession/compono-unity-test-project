# QuickCapture
QuickCapture is a Unity Editor tool designed to capture images from a list of prefabs. The QuickCapture tool also allows for the automatic rotation of each prefab to create a screenshot at various angles.

Item | Value
------------ | -------------
Game Engine | Unity
Unity Version | 2019.4.0f1

Created by Jameson Hartel 2020

## Quick Setup ##

1. [Install Unity Hub](https://unity3d.com/get-unity/download)
1. Choose personal license for activation
1. Under `Installs` install the Unity version specified in the above table
1. Clone this repo to your PC somewhere
1. Under `Projects` click `ADD` and select the `QuickCapture` folder
1. Open the project ensuring that the `Unity Version` field matches the Unity version specified in the above table

## Using QuickCapture ##
1. Open a scene and drag the QuickCapture prefab from `QuickCapture/Prefab` folder into the scene.
1. Create a SpawnItemList by right clicking in the Project view and selecting `Create>Export>SpawnItemList`.
1. Add assets that you want to screenshot to the newly created SpawnItemList.
1. Attach the newly created SpawnItemList to your QuickCapture prefab.
1. Adjust settings on the ExportRoutine script (found on the QuickCapture prefab) to your taste.
1. Ensure `Image Resolution` and `Output folder` are set.
1. Enter playmode and press `Start Capturing` on the ExportRoutine script.
