﻿/* 
 * Created by Jameson Hartel 2020
 * This script contains logic to capture 
 * and save transparent screenshots
 * from a given camera in Unity.
 */

using System.IO;
using UnityEngine;

public class ScreenShot : MonoBehaviour
{

    public Texture2D TakeScreenshot(string dirPath, Camera cam, int width, int height)
    {
        //Create RenderTexture
        RenderTexture rt = new RenderTexture(width, height, 32);

        //Setup cam
        cam.orthographic = true;
        cam.clearFlags = CameraClearFlags.Depth; //For transparent background ;)
        cam.targetTexture = rt;

        //Create texture for screenshot
        Texture2D screenShot = new Texture2D(width, height, TextureFormat.ARGB32, false);
        cam.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);

        //Cleanup
        cam.targetTexture = null;
        RenderTexture.active = null;
        Destroy(rt);

        //Save to disk
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = GenerateScreenshotPathName(dirPath);
        File.WriteAllBytes(filename, bytes);

        return screenShot;
    }

    private string GenerateScreenshotPathName(string dirPath)
    {
        // Name the screenshot based on number of images in folder
        return string.Format(dirPath + "/frame{0:D4}.png",
            Directory.GetFiles(dirPath, "*.png", SearchOption.TopDirectoryOnly).Length);
    }
}