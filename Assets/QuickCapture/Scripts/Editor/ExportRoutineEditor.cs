﻿/* 
 * Created by Jameson Hartel 2020
 * This script is used to style the ExportRoutine
 * script in the inspector to improve usability.
 */

using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(ExportRoutine))]
public class ExportRoutineEditor : Editor
{
    #region vars
    //Strip out from default inspector draw
    private static readonly string[] _dontIncludeMe = new string[] { "m_Script", "m_outputPath", "m_isCapturing"};

    //Logo
    [SerializeField] private Texture m_logo;
    private readonly float m_logoPadding = 30;
    private Rect m_logoPos = new Rect(0, 10, 0, 50);

    //Settings pane
    private string m_outputFolderRelPath;

    //Capture pane
    private Rect m_progressBarPos = new Rect(32, 260, 0, 20);

    //Misc
    private GUIStyle m_vertGuiStlye;
    private readonly int m_rightGlobalMargin = 15;
    #endregion

    public override void OnInspectorGUI()
    {
        #region init
        float viewWidth = EditorGUIUtility.currentViewWidth;
        m_vertGuiStlye = new GUIStyle("HelpBox");
        m_vertGuiStlye.margin.right = m_rightGlobalMargin;
        ExportRoutine exportRoutine = (ExportRoutine)target;
        //Set default preferences
        if (!EditorPrefs.HasKey("OutputPath") || !Directory.Exists(EditorPrefs.GetString("OutputPath")))
            EditorPrefs.SetString("OutputPath", Application.dataPath.Replace("/Assets", "") + "/Output");
        if (!EditorPrefs.HasKey("ImageResolutionHeight")) EditorPrefs.SetInt("ImageResolutionHeight", 512);
        if (!EditorPrefs.HasKey("ImageResolutionWidth")) EditorPrefs.SetInt("ImageResolutionWidth", 512);
        if (!EditorPrefs.HasKey("modelRotateDegrees")) EditorPrefs.SetFloat("modelRotateDegrees", 22.5f);
        if (!EditorPrefs.HasKey("modelRotateCount")) EditorPrefs.SetInt("modelRotateCount", 16);
        if (!EditorPrefs.HasKey("exitPlayMode")) EditorPrefs.SetBool("exitPlayMode", true);

        #endregion

        #region Logo
        EditorGUILayout.BeginVertical(m_vertGuiStlye);
        ExportRoutine exportRoutineScript = (ExportRoutine)target;
        m_logoPos.width = viewWidth - (m_logoPadding*2f);
        m_logoPos.x = m_logoPadding;
        Color guiColor = GUI.color; // Save the current GUI color
        GUI.color = Color.clear; // This does the magic
        if (m_logo != null)EditorGUI.DrawTextureTransparent(m_logoPos, m_logo, ScaleMode.ScaleToFit);
        GUI.color = guiColor; // Get back to previous GUI color
        EditorGUILayout.Space(55);
        EditorGUILayout.EndVertical();
        #endregion

        #region Settings pane
        EditorGUILayout.BeginVertical(m_vertGuiStlye);
        EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);

        #region Default inspector properties
        serializedObject.Update();
        DrawPropertiesExcluding(serializedObject, _dontIncludeMe);
        serializedObject.ApplyModifiedProperties();
        #endregion

        EditorGUILayout.BeginHorizontal();

        GUIStyle gs = new GUIStyle(GUI.skin.textField)
        {
            wordWrap = false,
            clipping = TextClipping.Clip
        };
        gs.margin.left = 5;
        EditorGUILayout.PrefixLabel("Output Folder:");

        string projectRoot = Application.dataPath.Replace("/Assets", "");
        if (EditorPrefs.GetString("OutputPath").StartsWith(projectRoot))
            m_outputFolderRelPath = EditorPrefs.GetString("OutputPath").Substring(projectRoot.Length);

        //Open folder selection window
        if (GUILayout.Button(m_outputFolderRelPath, gs))
        {
            EditorPrefs.SetString("OutputPath", EditorUtility.OpenFolderPanel("Select output folder", "", ""));
            GUIUtility.ExitGUI();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Image Resolution:");
        EditorPrefs.SetInt("ImageResolutionHeight", IntField("H", EditorPrefs.GetInt("ImageResolutionHeight")));
        EditorPrefs.SetInt("ImageResolutionWidth", IntField("W", EditorPrefs.GetInt("ImageResolutionWidth")));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 0;
        EditorGUILayout.PrefixLabel("Degrees to rotate model by:");
        EditorPrefs.SetFloat("modelRotateDegrees", FloatField("", EditorPrefs.GetFloat("modelRotateDegrees")));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 0;
        EditorGUILayout.PrefixLabel("Count to rotate model by:");
        EditorPrefs.SetInt("modelRotateCount", IntField("", EditorPrefs.GetInt("modelRotateCount")));
        EditorGUILayout.EndHorizontal();

        EditorGUIUtility.labelWidth = 0;
        EditorPrefs.SetBool("exitPlayMode", EditorGUILayout.Toggle("Exit playmode on completion", EditorPrefs.GetBool("exitPlayMode")));

        EditorGUILayout.EndVertical();
        #endregion

        #region Capture pane
        EditorGUILayout.BeginVertical(m_vertGuiStlye);
        EditorGUILayout.LabelField("Capture", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Hint: Start play mode to enable capturing");

        m_progressBarPos.width = viewWidth - 65;
        EditorGUI.ProgressBar(m_progressBarPos, exportRoutine.GetCaptureProgressValue(), exportRoutine.GetCaptureProgressText());
        EditorGUILayout.Space(27);

        GUIStyle capBtnStlye = new GUIStyle(GUI.skin.button);
        capBtnStlye.margin.left = (int)viewWidth / 2 - (100);
        capBtnStlye.margin.right = m_rightGlobalMargin;

        string IsCapturing = (exportRoutine.GetCaptureState() ? "Stop" : "Start") + " Capturing";
        GUI.enabled = EditorApplication.isPlaying;
        if (GUILayout.Button(IsCapturing, capBtnStlye, GUILayout.Width(200)))
        {
            if (exportRoutine.GetCaptureState())
            {
                exportRoutine.StopCapture();
            } else
            {
                exportRoutine.StartCapture();
            }
        }

        EditorGUILayout.Space(2);

        EditorGUILayout.EndVertical();
        #endregion
        
    }

    /// <summary>
    /// Create a EditorGUILayout.TextField with no space between label and text field
    /// </summary>
    private static int IntField(string label, int num)
    {
        var textDimensions = GUI.skin.label.CalcSize(new GUIContent(label));
        EditorGUIUtility.labelWidth = textDimensions.x;
        GUIStyle txtFieldGuiStyle = new GUIStyle(GUI.skin.textField);
        txtFieldGuiStyle.margin.left = 5;
        txtFieldGuiStyle.wordWrap = false;
        txtFieldGuiStyle.clipping = TextClipping.Overflow;
        return EditorGUILayout.IntField(label, num, txtFieldGuiStyle);
    }

    /// <summary>
    /// Create a EditorGUILayout.TextField with no space between label and text field
    /// </summary>
    private static float FloatField(string label, float num)
    {
        var textDimensions = GUI.skin.label.CalcSize(new GUIContent(label));
        EditorGUIUtility.labelWidth = textDimensions.x;
        GUIStyle txtFieldGuiStyle = new GUIStyle(GUI.skin.textField);
        txtFieldGuiStyle.margin.left = 5;
        txtFieldGuiStyle.wordWrap = false;
        txtFieldGuiStyle.clipping = TextClipping.Overflow;
        return EditorGUILayout.FloatField(label, num, txtFieldGuiStyle);
    }
}
