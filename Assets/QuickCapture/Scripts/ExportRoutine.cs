﻿/* 
 * Created by Jameson Hartel 2020
 * This script contains logic to automate 
 * the process for capturing transparent images
 * of prefabs.
 */

#if (UNITY_EDITOR)
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(ScreenShot))]
public class ExportRoutine : MonoBehaviour
{   
    private ScreenShot m_screenShot;
    [SerializeField] private SpawnItemList m_itemList = null;

    private AssetReferenceGameObject m_assetLoadedAsset;
    private GameObject m_instanceObject = null;
    
    private int m_currentAssetPosition = 0;
    private string m_dirPath = "";
    private Camera m_mainCamera = null;

    //Capture stuff
    private bool m_stopCapture = false;
    private bool m_isCapturing = false;
    private bool m_doCapture = false;

    //ImagePreview stuff
    private TextMeshProUGUI m_statusText;
    private RawImage m_imagePreview;
    private RawImage[] m_imagePreviews;
    private readonly string m_idleString = "Status: Idle";
    private readonly string m_captureString = "Status: Prefab {0} out of {1}";

    private void Start()
    {
        m_imagePreview = GameObject.FindGameObjectWithTag("ImagePreview").GetComponent<RawImage>();
        m_statusText = GameObject.FindGameObjectWithTag("StatusText").GetComponent<TextMeshProUGUI>();
        m_mainCamera = Camera.main;
        m_screenShot = GetComponent<ScreenShot>();

        if (m_itemList == null || m_itemList.AssetReferenceCount == 0) 
        {
            Debug.LogError("Spawn list not setup correctly");
        } else if (m_isCapturing)
        {
            LoadItemAtIndex(m_itemList, 0);
        }
    }

    private void LateUpdate()
    {
        if (m_doCapture) {
            m_statusText.text = string.Format(m_captureString, m_currentAssetPosition, m_itemList.AssetReferenceCount);
            m_doCapture = false;
            CaptureRoutine();
        }
    }

    private void LoadItemAtIndex(SpawnItemList itemList, int index) 
    {
        if (m_instanceObject != null) 
        {
           Destroy(m_instanceObject);
        }       
        
        m_assetLoadedAsset = itemList.GetAssetReferenceAtIndex(index);
        var spawnPosition = new Vector3();
        var spawnRotation = Quaternion.identity;
        var parentTransform = this.transform;

        var loadRoutine = m_assetLoadedAsset.LoadAssetAsync();
        loadRoutine.Completed += LoadRoutine_Completed;

        void LoadRoutine_Completed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
        {
            //Create path for prefab
            m_dirPath = EditorPrefs.GetString("OutputPath") + "/" + obj.Result.name;
            if (!Directory.Exists(m_dirPath))Directory.CreateDirectory(m_dirPath);

            m_instanceObject = Instantiate(obj.Result, spawnPosition, spawnRotation, parentTransform);
            FitCameraToGameobject(m_instanceObject);
            m_doCapture = true;
        }
    }

    public void StartCapture()
    {
        if (m_itemList == null || m_itemList.AssetReferenceCount == 0)
        {
            Debug.LogError("Spawn list not setup correctly");
        } else if (m_mainCamera == null)
        {
            Debug.LogError("Couldn't find camera tagged as MainCamera in scene");
        } else {
            m_stopCapture = false;
            m_isCapturing = true;
            //Ensure Camera is centered
            m_mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, m_mainCamera.transform.position.z);
            CaptureSetupAndStart();
        }
    }

    private void CaptureSetupAndStart()
    {

        //Clear unused copies of images
        if(m_imagePreviews != null)
            foreach (RawImage imgPreview in m_imagePreviews)
                if (imgPreview != m_imagePreview) Destroy(imgPreview.gameObject);

        //Initialise array
        m_imagePreviews = new RawImage[EditorPrefs.GetInt("modelRotateCount")];
        float groupPadding = m_statusText.gameObject.GetComponent<RectTransform>().anchorMin.y;
        float imgMargin = 0.05f;
        int nRow = (int)Mathf.Ceil(Mathf.Sqrt(EditorPrefs.GetInt("modelRotateCount")));
        float imgSize = (groupPadding - imgMargin)/nRow;
        Vector2 anchorMin = new Vector2(imgMargin, groupPadding - imgSize);
        Vector2 anchorMax = new Vector2(imgSize, groupPadding - imgMargin);

        for (int i = 0; i < EditorPrefs.GetInt("modelRotateCount"); i++)
        {
            if(i == 0)
            {
                m_imagePreviews[i] = m_imagePreview;
            } else {
                GameObject ImgPreview = Instantiate(m_imagePreview.gameObject, m_imagePreview.gameObject.transform.position, m_imagePreview.gameObject.transform.rotation, m_imagePreview.gameObject.transform.parent) as GameObject;
                m_imagePreviews[i] = ImgPreview.GetComponent<RawImage>();
            }
            
            //Set RectTransform Anchor
            RectTransform imgPreviewRect = m_imagePreviews[i].GetComponent<RectTransform>();
            imgPreviewRect.anchorMin = anchorMin;
            imgPreviewRect.anchorMax = anchorMax;

            anchorMin.x += imgSize;
            anchorMax.x += imgSize;
            if(anchorMax.x >= 1) {
                anchorMin.x = imgMargin;
                anchorMax.x = imgSize;
                anchorMin.y -= imgSize;
                anchorMax.y -= imgSize;
            }
        }

        LoadItemAtIndex(m_itemList, 0);
    }

    public string GetCaptureProgressText()
    {
        return m_isCapturing ? string.Format("Capturing: {0}/{1}", m_currentAssetPosition, m_itemList.AssetReferenceCount) : "Idle";
    }

    public float GetCaptureProgressValue()
    {
        return m_isCapturing ? m_currentAssetPosition/(float)m_itemList.AssetReferenceCount : 0;
    }

    public bool GetCaptureState()
    {
        return m_isCapturing;
    }

    public void StopCapture()
    {
        m_stopCapture = true;
    }

    private void CaptureRoutine()
    {
        //Roate model
        for (int i = 0; i < EditorPrefs.GetInt("modelRotateCount"); i++)
        {
            m_instanceObject.transform.Rotate(0, EditorPrefs.GetFloat("modelRotateDegrees"), 0);
            //Take screenshot
            Texture2D lastCapture = m_screenShot.TakeScreenshot(m_dirPath, m_mainCamera,
                EditorPrefs.GetInt("ImageResolutionWidth"),
                EditorPrefs.GetInt("ImageResolutionHeight"));
            lastCapture.Apply();
            m_imagePreviews[i].texture = lastCapture;
        }

        if (m_currentAssetPosition < m_itemList.AssetReferenceCount && !m_stopCapture)
        {
            LoadItemAtIndex(m_itemList, m_currentAssetPosition++);
        }
        else
        {
            m_currentAssetPosition = 0;
            m_isCapturing = false;
            m_statusText.text = m_idleString;
            if (EditorPrefs.GetBool("exitPlayMode")) EditorApplication.isPlaying = false;
        }
    }
    
    private void FitCameraToGameobject(GameObject GameObjectToFit)
    {
        Bounds bounds = new Bounds();
        MeshFilter[] meshFilters = GameObjectToFit.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter meshFilter in meshFilters)
        {
            bounds.Encapsulate(meshFilter.mesh.bounds);
        }

        m_mainCamera.transform.position =
            new Vector3(m_mainCamera.transform.position.x, bounds.center.y, m_mainCamera.transform.position.z);
        m_mainCamera.orthographicSize = bounds.size.magnitude/2;
    }
}
#endif